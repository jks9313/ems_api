$(document).ready(function(){
	var agent = navigator.userAgent.toLowerCase();
	
	$('.intOnly').bind('keyup keydown',function(){
		 inputNumberFormat(this);
	});

	
	
	//네비 및 뒤로가기 버튼에 클래스 추가해주세요.
	jQuery('.doc_back_btn').bind('click', function(){
		window.history.go(-1);
	});
	//데이터피커 활용시 입력필드에 클래스 추가해주세요
	jQuery('.isDatePicker').datepicker({
        dateFormat: 'yy년 mm월 dd일',
        prevText: '<i class="fa fa-chevron-left"></i>',
        nextText: '<i class="fa fa-chevron-right"></i>',
        onSelect:function() {
        	  //bootstrap validate
        		   jQuery(this).parent().find('small').attr('data-bv-result', 'VALID');
	           jQuery(this).parent().find('small').css('display', 'none');
	           jQuery(this).parent().parent().removeClass('has-error');
	           jQuery(this).parent().parent().addClass('has-success');
        }
    });
	//readonly 영역 회색배경 지우기
	jQuery('.removeDate').css('cursor','pointer');
	jQuery('.isDatePicker').css('background-color', 'white');
	jQuery('.address').css('background-color', 'white'); 
	jQuery('.zipcode').css('background-color', 'white');
	jQuery('.address2').css('background-color', 'white'); 
	jQuery('.zipcode2').css('background-color', 'white');
	jQuery('.isModal').css('background-color', 'white');

	//datepicker input 밸류 지우기
	jQuery('.removeDate').bind('click', function(){
		$(this).parent().find('input').val('');
	});
	
	 jQuery('a').bind('click', function(){
  		switch(this.id){
  			case "productWriteBtn": document.location.href="/commonInfo/productWrite";	break;
  			case "userWriteBtn": document.location.href="/commonInfo/userWrite";	break;
  			case "clientWriteBtn": document.location.href="/commonInfo/clientWrite";	break;
  			case "breakdownWriteBtn": document.location.href="/commonInfo/breakdownWrite"; break;
  			case "faultyWriteBtn": document.location.href="/commonInfo/faultyWrite"; break;
  			case "nonOperationTimeWriteBtn": document.location.href="/commonInfo/nonOperationTimeWrite"; break;
  			case "taskTitleWriteBtn": document.location.href="/commonInfo/taskTitleWrite"; break;
  			case "estimateWriteBtn": document.location.href="/business/estimateWrite"; break;
  			case "materialOrderWriteBtn": document.location.href="/material/orderWrite"; break;
  			case "estimateDocWriteBtn": document.location.href="/business/estimateDocWrite"; break;
  			case "oredrWriteBtn": document.location.href="/business/orderWrite"; break;
  			case "materialArrivalWriteBtn": document.location.href="/materialHistory/materialArrivalWrtie"; break;
  		}
	 });
	 //사진파일이 아닌경우 
	 jQuery('.isNormalFile').on('change', function(){
			$(this).parent().find('p').css('display', 'inline');
	 });
	 //modal띄우는 인풋박스 css추가
	 jQuery('.isModal').css('cursor', 'pointer');
	 jQuery('.zipcode').css('cursor', 'pointer');
	 jQuery('.zipcode2').css('cursor', 'pointer');
		
	 //사진파일이 아닌경우 코멘트
	 jQuery('.isResetFileBtn').bind('click', function(){
		if ( (navigator.appName == 'Netscape' && navigator.userAgent.search('Trident') != -1) || (agent.indexOf("msie") != -1) ) {
			var file		=	$(this).parent().parent().find('input');
			$(file).replaceWith( $(file).clone(true) );
		}
		else {
			$(this).parent().parent().find('input').val("");
		}
			$(this).parent().css('display', 'none');		
	 });
	
});	


//주소찾기 활용시 입력필드에 맞는 클래스 추가해주세요
function postCode() {
    new daum.Postcode({
        oncomplete: function(data) {
        		var roadAddr = data.roadAddress; // 도로명 주소 변수
        		var zipcode	=	data.zonecode;
          
			   jQuery('.address').val(roadAddr)
	           jQuery('.zipcode').val(zipcode);
	
	            var guideTextBox = document.getElementById("guide");
	           //자동입력시 bootstrapValidation 미적용으로 인해 직접 적용 
	           jQuery('.zipcode').parent().find('small').attr('data-bv-result', 'VALID');
	           jQuery('.zipcode').parent().find('small').css('display', 'none');
	           jQuery('.zipcode').parent().parent().removeClass('has-error');
	           jQuery('.zipcode').parent().parent().addClass('has-success');
	       
	           jQuery('.address').parent().find('small').attr('data-bv-result', 'VALID');
	           jQuery('.address').parent().find('small').css('display', 'none');
	           jQuery('.address').parent().parent().removeClass('has-error');
	           jQuery('.address').parent().parent().addClass('has-success');
        }
    }).open();
}

//주소찾기가 2개 이상일때
function postCode2() {
	new daum.Postcode({
		oncomplete: function(data) {
			var roadAddr = data.roadAddress; // 도로명 주소 변수
			var zipcode	=	data.zonecode;
			
			jQuery('.address2').val(roadAddr)
			jQuery('.zipcode2').val(zipcode);
			
			var guideTextBox = document.getElementById("guide");
			//자동입력시 bootstrapValidation 미적용으로 인해 직접 적용 
			jQuery('.zipcode2').parent().find('small').attr('data-bv-result', 'VALID');
			jQuery('.zipcode2').parent().find('small').css('display', 'none');
			jQuery('.zipcode2').parent().parent().removeClass('has-error');
			jQuery('.zipcode2').parent().parent().addClass('has-success');
			
			jQuery('.address2').parent().find('small').attr('data-bv-result', 'VALID');
			jQuery('.address2').parent().find('small').css('display', 'none');
			jQuery('.address2').parent().parent().removeClass('has-error');
			jQuery('.address2').parent().parent().addClass('has-success');
		}
	}).open();
}

//금액입력시 3자리수 자동 콤마 및 한글방지
function numberFormat(obj) {
	 obj.value = comma(uncomma(obj.value));
}


function comma(str) {
    str = String(str);
    return str.replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,');
}
function uncomma(str) {
    str = String(str);
    return str.replace(/[^\d]+/g, '');
}
