$(document).ready(function(){
	
	$('.floatOnly').keydown(function(event){
		
		
		var num_check=/^[+-]?\d*(\.?\d*)$/;
		var hangle_check = /[ㄱ-ㅎ|ㅏ-ㅣ|가-힣]/;
		var inputKey  = event.key;
		if(inputKey.length > 1)inputKey =  '';
		
		if(hangle_check.test(inputKey )){
			$(this).val($(this).val().replace(/[ㄱ-ㅎ|ㅏ-ㅣ|가-힣]/g, ''))
			return false;
		}
		
		if(!num_check.test($(this).val()+inputKey )){
			return false;
		}
		return true;
		
	});
	$('.floatOnly').css("ime-mode", "disabled");
	
	$('.intOnly').bind('keyup keydown',function(){
		 inputNumberFormat(this);
	});
	$('.historyBack').on('click', function(){
		window.history.back();
	});
//	$('.intOnly').keydown(function(event){
//		var num_check=/^[+-]?\d*(\.?\d*)$/;
//		var inputKey  = event.key;
//		if(inputKey.length > 1)inputKey =  '';
//		alert(inputKey);
//		
//		if(!num_check.test($(this).val()+inputKey )){
//			return false;
//		}
//		return true;
//		
//	});
	
	
	$('.phoneNumber').bind("keydown keyup", function(event){

		var num_check=/^\d*(\.?\d*)$/;
		var hangle_check = /[ㄱ-ㅎ|ㅏ-ㅣ|가-힣]/;
		var inputKey  = event.key;
		if(inputKey.length > 1)inputKey =  '';
		
		if(hangle_check.test(inputKey )){
			$(this).val($(this).val().replace(/[ㄱ-ㅎ|ㅏ-ㅣ|가-힣]/g, ''))
			return false;
		}
		var phoneNumber = $(this).val().replace(/\-/g,'');
		if(!num_check.test(phoneNumber+inputKey )){
			return false;
		}
		
		$(this).val(phoneNumber.replace(/(^02.{0}|^01.{1}|[0-9]{3})([0-9]+)([0-9]{4})/,"$1-$2-$3"));
		
		if((phoneNumber+inputKey ).length > 11) return false;
		
		
	});
	$('.regDate').bind("keydown keyup", function(event){
		
		var num_check=/^\d*(\.?\d*)$/;
		var hangle_check = /[ㄱ-ㅎ|ㅏ-ㅣ|가-힣]/;
		var inputKey  = event.key;
		if(inputKey.length > 1)inputKey =  '';
		
		if(hangle_check.test(inputKey )){
			$(this).val($(this).val().replace(/[ㄱ-ㅎ|ㅏ-ㅣ|가-힣]/g, ''))
			return false;
		}
		var phoneNumber = $(this).val().replace(/\-/g,'');
		if(!num_check.test(phoneNumber+inputKey )){
			return false;
		}
		console.log(phoneNumber);
		$(this).val(phoneNumber.replace(/([0-9]+)([0-9]{2})([0-9]{2})/,"$1-$2-$3"));
		
		if((phoneNumber+inputKey ).length > 8) return false;
		
		
	});
	
	
	  $('.uploadFile').on('change', function(){
			fileCheck(this);
		}); 
	  $('.removeDate').css('cursor', 'pointer');
	  $('.removeDate').css('color', 'gray');
	  
	  $('.removeDate').on('click', function(){
		  $(this).parent().find('input').val('');
	  });
	
});



function fileCheck(file){
	var submitExec	=	true;
	//file size CheckValue
	var maxSize		=	500 * 1024 * 20; //500kb 추후에 용량체크 가능
	var fileSize	=	0;
	
	//file ext CheckValue
	var fileExt = file.value.match(/\.(.+)$/)[1].toLowerCase();
	var fileContentsType;
	
	var browser		=	navigator.appName;
	//브라우져 체크
	if(browser		==	"Microsoft Internet Explorer"){
		var fileInfo		=	new ActiveXObject("scripting.FileSystemObject");
		fileSize			=	fileInfo.getFile(	file.value	).size;
		fileContentsType	=	fileInfo.getFile(	file.value	).type;
	} else {
		fileSize			=	file.files[0].size;
		fileContentsType	=	file.files[0].type;
	}
	if((fileSize	 >	maxSize) &&	(submitExec	==	true))	{
		alert("첨부파일 사이즈는 2mb 이내로 등록가능합니다");
		$('#agencyFile').val('');
		return;
	}
	if(submitExec	==	true) {
		switch(fileExt)	{
			case	'PNG	'	:
			case	'jpg'	:
			case	'jpeg'		:	 submitExec	=	true;	break;
			default			: 	 alert("지원하지 않는 헝식입니다");	jQuery(file).val('');	break;
		}
	}
}

function blockInput(ev, obj){
	
	if(event.keyCode==9||event.keyCode==37||event.keyCode==39||event.keyCode==46) return;
	obj.vlaue=obj.value.replace(/[\ㄱ-ㅎ-|가-힣]/gi,'');
	ev.preventDefault();
}


function jusoCallBack(roadFullAddr,roadAddrPart1,addrDetail,roadAddrPart2,engAddr, jibunAddr, zipNo, admCd, rnMgtSn, bdMgtSn
		, detBdNmList, bdNm, bdKdcd, siNm, sggNm, emdNm, liNm, rn, udrtYn, buldMnnm, buldSlno, mtYn, lnbrMnnm, lnbrSlno, emdNo){
	var jibun = lnbrMnnm;
	if(lnbrSlno!=''){
		jibun = jibun + '-' + lnbrSlno
	}
	if(addrDetail!=''){
		$("#rn").val(rn+' '+addrDetail);
	}
		$('#postalCode').val(zipNo);
		$('#jibunFullAddr').val(siNm +" "+ sggNm + " "+emdNm + " " +jibun+" "+addrDetail);
		$('#roadFullAddr').val(roadFullAddr);
		$("#siNm").val(siNm);
		$("#sggNm").val(sggNm);
		$("#emdNm").val(emdNm);
		$("#liNm").val(liNm);
		$("#jibun").val(jibun);
		$("#rn").val(roadFullAddr);
		$('#buildingName').val(bdNm);
		
		contractNameAutoComplete();
}

	function jusoCall(){
		$('#searchAddr').trigger('click');
	}	

	function jusoCallBack2(roadFullAddr,roadAddrPart1,addrDetail,roadAddrPart2,engAddr, jibunAddr, zipNo, admCd, rnMgtSn, bdMgtSn
			, detBdNmList, bdNm, bdKdcd, siNm, sggNm, emdNm, liNm, rn, udrtYn, buldMnnm, buldSlno, mtYn, lnbrMnnm, lnbrSlno, emdNo){
			var jibun = lnbrMnnm;
			if(lnbrSlno!=''){
			jibun = jibun + '-' + lnbrSlno
			}
			if(addrDetail!=''){
			$("#rn").val(rn+' '+addrDetail);
			}
			$('#groupsJibunFullAddr').val(siNm +" "+ sggNm + " "+emdNm + " " +jibun+" "+addrDetail);
			$('#groupsRoadFullAddr').val(roadFullAddr);
			$("#groupsSiNm").val(siNm);
			$("#groupsSggNm").val(sggNm);
			$("#groupsEmdNm").val(emdNm);
			$("#groupsLiNm").val(liNm);
			$("#groupsJibun").val(jibun);
	}
	function contractNameAutoComplete(){
		var buildingName	=	$('#buildingName').val();
		var contractDiv	=	$('#contractDivText').val();
		if(contractDiv !=	0){
			$('#contractName').val(buildingName + " "+contractDiv);
		} else{
			
			var inspectionDiv	=	$('#inspectionDiv option:selected').val();
		  	if(inspectionDiv ==	1){
				$('#contractName').val(buildingName + " 작동기능점검");
		  	}else if (inspectionDiv ==	2){
				$('#contractName').val(buildingName + " 종합정밀점검");
		  	}else{
				$('#contractName').val(buildingName + " 종합+작동");
		  	}
		}
	}
	//blur 시 문자열 삭제 체크 및 음수 체크
	function onlyIntChk(selector){
		if($(selector).val()==''){
			return false;
		}
		
		var price	=	$(selector).val().replace(/[^\d]+/g, '');
		var unPaid	=	parseInt(price);
		if(unPaid<0){
			alert('금액이 0보다 작을수 없습니다.')
			$(selector).val('');
			return false;
		}
		unPaid = String(unPaid);
		unPaid = unPaid.replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,');
			$(selector).val(unPaid);
	}
	// 금액입력시 3자리수 자동 콤마 및 한글방지
	function inputNumberFormat(obj) {
		 obj.value = comma(uncomma(obj.value));
	}
	function comma(str) {
	    str = String(str);
	    return str.replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,');
	}
	function uncomma(str) {
	    str = String(str);
	    return str.replace(/[^\d]+/g, '');
	}
