<%@page import="com.nori.hockey.domain.Pdf"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">

<head style="margin: 0; padding: 0;">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>개인정보 활용동의서</title>
</head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	 	var oSerializer = new XMLSerializer();
		var xmlString4 = oSerializer.serializeToString($("#pdfArea")[0]);
		var name = $("#name").val();
		var date = $("#date").val();
		$.ajax({
		url : "/convertPDF",
		type: 'post',
		data : {
			'html' : xmlString4
			,'name' : name
			,'date' : date
			},
		success : function(code) {
			location.href=code;
		}, error:function(request,status,error){
			 alert('일지생성 실패! 관리자에게 문의해주세요.');
		},


		});       
	});
</script>

<body style="margin: 0; padding: 0;"  >
<%
Pdf pdf = (Pdf)request.getAttribute("pdf");
%>
	<input type="hidden" id="seq" value="<%=pdf.getSeqNo()%>" name="seq">
	<input type="hidden" id="name" value="<%=pdf.getRegName()%>" name="name">
	<input type="hidden" id="date" value="<%=pdf.getRegDate()%>" name="date">
	<div id="pdfArea" style="font-family: Nanum;">

    <div class="container" style="margin: 0; padding: 0; width: 794px; margin: auto;">
        <p style="font-weight: bold; font-size: 1.2em; text-align: center; padding-bottom: 20px">Web Symposium 개인정보 동의서 (필수 항목)</p>
        <div style="width: 100%; max-width: 800px;">
            <p style="font-size: 0.8em; line-height: 25px; text-align: justify; padding-bottom: 20px;"><span style="font-weight: bold;">한국화이자제약 주식회사</span>(이하 총칭하여 “<span style="font-weight: bold;">회사</span>”)는 [Web Symposium](이하 ‘본 웹심포지엄’)을 진행할 예정입니다. 회사는 본 웹심포지엄의 참가 안내 및 관련 서비스를 수행하기 위해 아래와 같이 귀하의 개인정보를 수집 및 이용합니다.</p>
        </div>
        <div style="margin: 0; padding: 0; width: 100%; max-width: 800px; margin: auto;">
            <table border="1" style="border-collapse: collapse;">
                <thead>
                    <tr>
                        <td style="font-size: 0.8em; text-align: center; width: 33.3333%; padding: 5px 0;">수집항목</td>
                        <td style="font-size: 0.8em; text-align: center; width: 33.3333%; padding: 5px 0;">수집 및 이용 목적</td>
                        <td style="font-size: 0.8em; text-align: center; width: 33.3333%; padding: 5px 0;">이용 및 보유기간</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td align="left" valign="top" style="font-size: 0.8em; padding: 10px; text-align: justify; line-height: 18px;">
                            <p>·&nbsp;성명, 연락처, 전자우편주소, 병원명, 과명</p>
                        </td>
                        <td align="left" valign="top" style="font-size: 0.8em; padding: 10px; text-align: justify; line-height: 18px;">
                            <p>·&nbsp;본 웹심포지엄 참가 및 관련 문의사항 안내</p>
                            <p>·&nbsp;본 웹심포지엄를 통한 서비스 제공</p>
                            <p>·&nbsp;본 웹심포지엄 참석 여부 확인 및 증빙</p>
                            <p>·&nbsp;본 웹심포지엄 종료 후 영업사원 방문 또는 연락 동의</p>
                            <p>·&nbsp;웹사이트 이용에 따른 민원사항의 처리 및 고지사항의 전달, 문의, 민원 및 답변 처리, 분쟁 대응, 제품에 대한 불만처리 기록 보관 및 부작용 보고, 회원의 서비스 이용현황 분석, 웹사이트 개선 및 사이트 사용 추세 파악</p>
                        </td>
                        <td align="left" valign="top" style="padding: 10px;">
                            <p style="font-size: 0.8em;">회사는 <span style="font-size: 16px; font-weight: bold; text-decoration:underline; padding-bottom: 2px; line-height: 28px;">관계 법령의 규정에 따라 귀하의 개인정보를 계속 보존할 의무가 있는 경우가 아닌 한, 본 웹심포지엄 당일이 속한 회계연도 종료 후 5년까지까지 귀하의 개인정보를 보유 및 이용합니다.</span></p>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div style="width: 100%; max-width: 800px;">
            <p style="font-size: 0.8em; line-height: 25px; text-align: justify; padding: 20px 0;">귀하는 동의를 거부할 권리가 있으나, 동의하지 않을 경우 본 웹심포지엄의 참가 및 관련 문의사항에 대한 원활한 처리 등이 이루어지지 않을 수 있습니다. 귀하는 동의한 이후에도 언제든지 이를 철회할 수 있습니다.</p>
        </div>

        <div style="margin: 0; padding: 0; width: 100%; max-width: 800px; margin: auto;">
            <table style="border: 1px solid #000000; background: #E6E6E6; width: 100%;">
                <thead>
                    <tr>
                        <td colspan="2" style="font-weight: bold; font-size: 0.8em; text-align: center; padding: 5px 0;">본인은 회사의 개인정보 수집 및 이용에 관한 설명을 모두 이해하였고, 이에</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td colspan="2" valign="top" style="font-size: 0.8em; padding: 5px 20px 10px 0;">
                            <p align="center" style="font-weight: bold; margin: 0;"><input type="checkbox" checked style="vertical-align: middle; margin-right: 5px;">동의합니다</p>
                        </td>
                </tr>
                </tbody>
            </table>
        </div>

        <div style="width: 100%; max-width: 800px;">
            <p style="font-weight: bold; font-size: 0.8em; line-height: 25px; text-align: justify; padding: 20px 0 0;">·&nbsp;개인정보 취급 위탁에 대한 안내</p>
            <p style="font-size: 0.8em; line-height: 25px; text-align: justify; padding-bottom: 20px;">회사는 본 웹심포지엄의 운영 및 관련 서비스의 원활한 제공을 위하여 귀하의 개인정보 처리를 외부 전문 업체에 위탁하고 있습니다. 회사의 개인정보 처리위탁 사항은 회사 홈페이지(https://www.pfizer.co.kr/)에 공개된 개인정보처리방침 제 4조(개인정보 처리업무의 위탁)에 자세히 나와있으므로 해당 내용 참고하시기 바랍니다.</p>

            <p style="font-size: 0.8em; line-height: 40px; text-align: justify;">&#60;개인정보 입력&#62;</p>
        </div>

        <div style="margin: 0; padding: 0; width: 100%; max-width: 800px; margin: auto;">
            <table border="1" style="border-collapse: collapse; width: 100%;">
                <tbody>
                    <tr>
                        <td style="background: #E7E6E6; font-weight: bold; font-size: 0.8em; width: 25%; padding: 5px 0 5px 5px;">성명<sup>*</sup></td>
                        <td style="font-size: 0.8em; width: 25%; padding: 5px 0 5px 5px;"><%= pdf.getRegName() %></td>
                        <td style="background: #E7E6E6; font-weight: bold; font-size: 0.8em; width: 25%; padding: 5px 0 5px 5px;">진료과목</td>
                        <td style="font-size: 0.8em; width: 25%; padding: 5px 0 5px 5px;"><%= pdf.getRegSpecialty() %></td>
                    </tr>
                    <tr>
                        <td style="background: #E7E6E6; font-weight: bold; font-size: 0.8em; width: 25%; padding: 5px 0 5px 5px;">병원명<sup>*</sup></td>
                        <td style="font-size: 0.8em; width: 25%; padding: 5px 0 5px 5px;"><%= pdf.getRegHospital() %></td>
                        <td style="background: #E7E6E6; font-weight: bold; font-size: 0.8em; width: 25%; padding: 5px 0 5px 5px;">근무지 전화번호</td>
                        <td style="font-size: 0.8em; width: 25%; padding: 5px 0 5px 5px;"><%= pdf.getRegCompPhone() %></td>
                    </tr>
                    <tr>
                        <td style="background: #E7E6E6; font-weight: bold; font-size: 0.8em; width: 25%; padding: 5px 0 5px 5px;">병원주소</td>
                        <td colspan="3" style="font-size: 0.8em; width: 25%; padding: 5px 0 5px 5px;"><%= pdf.getRegHospitalAddress() %></td>
                    </tr>
                    <tr>
                        <td style="background: #E7E6E6; font-weight: bold; font-size: 0.8em; width: 25%; padding: 5px 0 5px 5px;">핸드폰 번호<sup>*</sup></td>
                        <td style="font-size: 0.8em; width: 25%; padding: 5px 0 5px 5px;"><%= pdf.getRegHp() %></td>
                        <td style="background: #E7E6E6; font-weight: bold; font-size: 0.8em; width: 25%; padding: 5px 0 5px 5px;">전자우편 주소<sup>*</sup></td>
                        <td style="font-size: 0.8em; width: 25%; padding: 5px 0 5px 5px;"><%= pdf.getRegEmail() %></td>
                    </tr>
                </tbody>
            </table>
            <p align="right" style="font-size: 0.7em; margin-top: 8px; padding-bottom: 40px; margin-bottom: 0;">*&nbsp;필수값</p>

            <div style="width: 100%; border-top: 1px solid #000000; clear: both; padding-bottom: 40px;"></div>
        </div>

        <p style="font-weight: bold; font-size: 1.2em; text-align: center; padding-bottom: 20px">개인정보 수집 및 이용 등에 관한 동의서</p>

        <div style="width: 100%; max-width: 800px;">
            <p style="font-weight: bold; font-size: 0.9em;">한국화이자제약 주식회사 및 한국화이자업존 주식회사 귀중</p>

            <p style="font-size: 0.8em; line-height: 25px; text-align: justify; padding-bottom: 20px;">본인은 아래의 내용을 확인하고, 한국화이자제약 주식회사(“<span style="font-weight: bold;">화이자업존</span>”) (이하 총칭하여 “<span style="font-weight: bold;">회사</span>”)가 다음과 같이 본인의 개인정보를 처리하는 것에 동의합니다.</p>

            <p style="font-weight: bold; font-size: 0.9em;">1.&nbsp;[선택] 영리목적 광고성 정보 수신 및 개인정보의 의ㆍ약학적 정보 전달 및 제품 홍보 등 마케팅 목적 수집 및 이용에 대한 동의</p>

            <p style="font-size: 0.8em; line-height: 25px; text-align: justify; margin-bottom: 0;">&#8226;&nbsp;회사는 다음과 같은 귀하의 정보를 수집하게 됩니다.</p>
            <p style="padding-left: 20px; font-size: 0.8em; line-height: 25px; text-align: justify; margin-top: 0;">&#9643;&nbsp;성명, 직종, 병원명, 병원 주소, 진료과목, 전화번호(근무지, 휴대폰), 전자우편주소</p>

            <p style="font-size: 0.8em; line-height: 25px; text-align: justify; margin-bottom: 0;">&#8226;&nbsp;회사가 귀하의 개인정보를 수집 및 이용하는 목적은 다음과 같습니다.</p>
            <p style="padding-left: 20px; font-size: 0.8em; line-height: 25px; text-align: justify; margin-top: 0; margin-bottom: 0;">&#9643;&nbsp;<span style="font-size: 16px; text-decoration:underline; padding-bottom: 1px; line-height: 25px;">전화, 이메일, SMS, 우편, 웹사이트, 모바일 앱, 원격 디테일링 등 다양한 온라인 및 오프라인 채널(“다양한 채널”)을 이용한, 온라인 및 오프라인 제품설명회를 포함한 보건의료전문가를 대상으로 하는 학술행사에 대한 안내 및 초대장 발송, 알림 서비스 제공 등의 마케팅 활동 수행</span></p>
            <p style="padding-left: 20px; font-size: 0.8em; line-height: 25px; text-align: justify; margin-top: 0; margin-bottom: 0;">&#9643;&nbsp;<span style="font-size: 16px; text-decoration:underline; padding-bottom: 1px; line-height: 25px;">다양한 채널을 통한 회사 제품의 안내를 포함한 의ㆍ약학적 정보 전달 및 취득 활동의 수행</span></p>
            <p style="padding-left: 20px; font-size: 0.8em; line-height: 25px; text-align: justify; margin-top: 0; margin-bottom: 0;">&#9643;&nbsp;<span style="font-size: 16px; text-decoration:underline; padding-bottom: 1px; line-height: 25px;">다양한 채널을 통한 기타 마케팅 정보의 수집, 전달 및 통계 산출</span></p>
            <p style="padding-left: 20px; font-size: 0.8em; line-height: 25px; text-align: justify; margin-top: 0; margin-bottom: 0;">&#9643;&nbsp;<span style="font-size: 16px; text-decoration:underline; padding-bottom: 1px; line-height: 25px;">다양한 채널을 통한 의학정보 문의사항에 대한 처리 및 기록</span></p>
            <p style="padding-left: 20px; font-size: 0.8em; line-height: 25px; text-align: justify; margin-top: 0;">&#9643;&nbsp;<span style="font-size: 16px; text-decoration:underline; padding-bottom: 1px; line-height: 25px;">의학정보 문의사항 및 웹사이트 이용에 대한 통계 산출</span></p>
            <p style="font-size: 0.8em; line-height: 25px; text-align: justify; margin-top: 0;">&#8226;&nbsp;회사는<span style="font-size: 16px; text-decoration:underline; padding-bottom: 1px; line-height: 25px;">&nbsp;관계 법령의 규정에 따라 귀하의 개인정보를 계속 보존할 의무가 있는 경우가 아닌 한, 이용자의 등록 탈퇴 시까지</span>&nbsp;귀하의 개인정보를 보유 및 이용합니다.</p>

            <p style="font-size: 0.8em; line-height: 25px; text-align: justify;">&#8226;&nbsp;귀하는 동의를 거부할 권리가 있으나, 동의하지 않을 경우 회사 제품의 안내, 의ㆍ약학적 정보 전달, 의학정보 문의사항에 대한 원활한 처리 등이 이루어지지 않을 수 있습니다. 귀하는 동의한 이후에도 언제든지 이를 철회할 수 있습니다.</p>
        </div>

        <div style="margin: 0; padding: 0; width: 100%; max-width: 800px; margin: auto;">
            <table style="border: 1px solid #000000; background: #E6E6E6; width: 100%;">
                <thead>
                    <tr>
                        <td colspan="2" style="font-weight: bold; font-size: 0.8em; text-align: center; padding: 5px 0;">본인은 회사의 개인정보 수집 및 이용에 관한 설명을 모두 이해하였고, 이에</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                    <% if(pdf.getSelectAgree1().equals("Y")){ %>
                    	<td colspan="2" valign="top" style="font-size: 0.8em; padding: 5px 20px 10px 0;">
                            <p style="font-weight: bold; margin: 0;" align="center"><input type="checkbox" checked style="vertical-align: middle; margin-right: 5px;">동의합니다</p>
                        </td>
                    <%}else{%>
                        <td colspan="2" valign="top" style="font-size: 0.8em; padding: 5px 0 10px 20px;">
                            <p  align="center" style="font-weight: bold; margin: 0;"><input type="checkbox" checked style="vertical-align: middle; margin-right: 5px;">동의하지 않습니다</p>
                        </td>
                   	<%}%>                   
                       
                    </tr>
                </tbody>
            </table>
        </div>

        <div style="width: 100%; max-width: 800px;">
            <p style="font-weight: bold; font-size: 0.9em;">2.&nbsp;개인정보의 의약학적 정보 전달 및 제품 홍보 등 마케팅 목적 처리위탁</p>

            <p style="font-size: 0.8em; line-height: 25px; text-align: justify;">&#8226;&nbsp;회사는 다음과 같이 개인정보 취급업무를 외부 업체에 위탁합니다.</p>
        </div>

        <div style="margin: 0; padding: 0; width: 100%; max-width: 800px; margin: auto;">
            <table border="1" style="border-collapse: collapse; width: 100%;">
                <thead>
                    <tr>
                        <td style="background: #E6E6E6; font-weight: bold; font-size: 0.8em; text-align: center; width: 50%; padding: 5px 0;">수탁자</td>
                        <td style="background: #E6E6E6; font-weight: bold; font-size: 0.8em; text-align: center; width: 50%; padding: 5px 0;">위탁하는 업무의 내용</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td align="left" valign="top" style="font-size: 0.8em; padding: 5px 0 5px 5px; text-align: justify;">
                            Epsilon
                        </td>
                        <td align="left" valign="top" style="font-size: 0.8em; padding: 5px 0 5px 5px; text-align: justify;">
                            이메일 발송 시스템 개발
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top" style="font-size: 0.8em; padding: 5px 0 5px 5px; text-align: justify;">
                            주식회사 카카오
                        </td>
                        <td align="left" valign="top" style="font-size: 0.8em; padding: 5px 0 5px 5px; text-align: justify;">
                            카카오 플러스친구 서비스 제공 목적의 개인정보 처리업무
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div style="width: 100%; max-width: 800px;">
            <p style="font-size: 0.8em; line-height: 25px; text-align: justify;">그 밖의 개인정보 처리위탁 사항은 회사 홈페이지<span style="color: #0000FF; text-decoration: underline;">(http://www.pfizer.co.kr/ko, 상기 홈페이지에서 화이자제약과 화이자업존의 개인정보 처리방침을 모두 확인하실 수 있습니다])</span>에 공개된 개인정보처리방침 제 4조(개인정보 처리업무의 위탁)에 자세히 나와있으므로 해당 내용 참고하시기 바랍니다.</p>
        </div>

        <div style="margin: 0; padding: 0; width: 100%; max-width: 800px; margin: auto;">
            <table style="border: 1px solid #000000; background: #E6E6E6; width: 100%;">
                <thead>
                    <tr>
                        <td colspan="2" style="font-weight: bold; font-size: 0.8em; text-align: center; padding: 5px 0;">본인은 회사의 개인정보 수집 및 이용에 관한 설명을 모두 이해하였고, 이에</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                    <% if(pdf.getSelectAgree1().equals("Y")){ %>
                    	<td colspan="2" valign="top" style="font-size: 0.8em; padding: 5px 20px 10px 0;">
                            <p align="center" style="font-weight: bold; margin: 0;"><input type="checkbox" checked style="vertical-align: middle; margin-right: 5px;">동의합니다</p>
                        </td>
                    <%}else{%>
                        <td colspan="2" valign="top" style="font-size: 0.8em; padding: 5px 0 10px 20px;">
                            <p align="center" style="font-weight: bold; margin: 0;"><input type="checkbox" checked style="vertical-align: middle; margin-right: 5px;">동의하지 않습니다</p>
                        </td>
                    <%}%>         
                        
                    </tr>
                </tbody>
            </table>
        </div>

        <div style="width: 100%; max-width: 800px;">
            <p style="font-size: 0.8em; line-height: 25px; text-align: justify;">개인정보 활용 동의 철회 문의: E-mail) Pfizer Korea, CO DCE (<span style="color: #0000FF; text-decoration: underline;">koreaemcm@pfizer.com</span>)</p>

            <p style="font-size: 0.8em; line-height: 25px; text-align: justify;">본인은 상기 내용을 상세히 읽어 보았고, 이에 관하여 충분히 이해하였으며, 본인의 자유로운 의사에 의해 명확히 동의하므로 아래와 같이 서명합니다.</p>

            <div style="margin: 0; padding: 0; width: 100%; float: left; padding-top: 30px;">
                <div style="margin: 0; padding: 0;">
                    <p align="right" style="margin: 0;margin-right: 3%; padding: 0;font-size: 1em; width: 100%;">
	                    서명 :
	                    <span style="margin: 0; padding: 0;width: 20%; display: block; float: right; font-weight:normal;"><%= pdf.getRegName() %></span>
                    </p>
                </div>
                <div style="margin: 0; padding: 20px 0 50px; clear: both;">
                    <p align="right" style="margin: 0; padding: 0;font-size: 1em; width: 100%;">
                    	날짜 : 
                    	<span style="margin: 0; padding: 0;width: 20%; display: block; float: right; font-weight:normal;"><%= pdf.getRegDate() %></span>
                    </p>
                </div>
            </div>
        </div>
    </div>
    </div>
</body>

</html>