package com.nori.hockey.file;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.SdkClientException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.DeleteObjectsRequest;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.drew.metadata.Directory;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@Service
public class S3Service {
	private AmazonS3 s3Client;

	private String accessKey = "AKIAZUJEWDFCKCQ7BVOB";
	private String secretKey = "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";
	private String bucket = "webinars-pdf";
	private String region = "ap-northeast-1";
	
//	String path = "/Users/jeong-gyeongsu/STSWork/PDFSERVER/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/webapps/upload/";
//	String path = "/var/lib/tomcat8/webapps/upload/";

	@PostConstruct
	public void setS3Client() {
		AWSCredentials credentials = new BasicAWSCredentials(this.accessKey, this.secretKey);

		s3Client = AmazonS3ClientBuilder.standard().withCredentials(new AWSStaticCredentialsProvider(credentials)).withRegion(this.region).build();
	}

	public String upload(MultipartFile file, String path, String fileName) throws IOException {
		String url = fileName;
		this.setS3Client();
		s3Client.putObject(new PutObjectRequest(bucket, url, file.getInputStream(), null).withCannedAcl(CannedAccessControlList.PublicRead));

		String downLink = s3Client.getUrl(bucket, url).toString();

		return downLink;
	}

	

}