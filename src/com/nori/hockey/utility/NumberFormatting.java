package com.nori.hockey.utility;

public class NumberFormatting {

	public String company_reg_number(String reg_number) {
		if(reg_number.trim().length() == 10) {
			String formating_reg_number	=	reg_number.substring(0,3);
			formating_reg_number	+=	"-";
			formating_reg_number	+=	reg_number.substring(3,5);
			formating_reg_number	+=	"-";
			formating_reg_number	+=	reg_number.substring(5,10);
			
			System.out.println(formating_reg_number);
			return formating_reg_number;
		}else {
			return reg_number;
		}
	}
	public String phone_number(String phone_number) {
		String formating_phone_number;
		if(phone_number.trim().length() == 11) {
			formating_phone_number	=	phone_number.substring(0,3);
			formating_phone_number	+=	"-";
			formating_phone_number	+=	phone_number.substring(3,7);
			formating_phone_number	+=	"-";
			formating_phone_number	+=	phone_number.substring(7,11);
			
			System.out.println(formating_phone_number);
			return formating_phone_number;
		} else if(phone_number.trim().length() == 10) {
			if(phone_number.startsWith("02")) {
				formating_phone_number 	= phone_number.substring(0,2);
				formating_phone_number	+=	"-";
				formating_phone_number	+=phone_number.substring(2,6);	
				formating_phone_number	+=	"-";
				formating_phone_number	+=phone_number.substring(6,10);	
				return formating_phone_number; 
			}else {
				formating_phone_number 	= phone_number.substring(0,3);
				formating_phone_number	+=	"-";
				formating_phone_number 	= phone_number.substring(3,6);
				formating_phone_number	+=	"-";
				formating_phone_number 	= phone_number.substring(6,10);
				return formating_phone_number; 
			}
		} else if(phone_number.trim().length() == 9) {
			formating_phone_number 	= phone_number.substring(0,2);
			formating_phone_number	+=	"-";
			formating_phone_number	+=phone_number.substring(2,5);	
			formating_phone_number	+=	"-";
			formating_phone_number	+=phone_number.substring(5,9);	
			return phone_number; 
		}
		else {
			return phone_number;
		}
	}
}
