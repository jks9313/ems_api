package com.nori.hockey.persistence.mysql.mybatis.mapper;

import java.util.List;

import com.nori.hockey.domain.Brand;
import com.nori.hockey.domain.Company;
import com.nori.hockey.domain.Member;
import com.nori.hockey.domain.Notice;


public interface NoticeMapper {
	
	List<Notice> selectListNotice();
	
	
}
