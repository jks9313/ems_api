package com.nori.hockey.persistence.mysql.mybatis.mapper;

import java.util.List;

import com.nori.hockey.domain.Brand;
import com.nori.hockey.domain.Company;
import com.nori.hockey.domain.Member;
import com.nori.hockey.domain.PreRegistration;
import com.nori.hockey.domain.Symposium;


public interface SymposiumMapper {
	
	List<Symposium> selectListSymposiumByUser(Symposium symposium);//1
	List<Symposium> selectListSymposiumByAdmin(Integer userSeq);//1
	Integer selectUserSeqByUserId(String userId);//1
	Symposium selectSymposium(Integer eventUuid);//1
	Integer insertPreRegTemp(PreRegistration preRegistration);//1
	Symposium selectOptionPreReg(Integer eventUuid);//1
	List<PreRegistration> selectListPreReg(Integer eventUuid);//0
	PreRegistration selectCountPreReg(Integer eventUuid);//0
	Integer selectOverlapCountPasscodeTemp(Integer eventUuid);
	Integer selectOverlapCountPasscode(Integer eventUuid);
	Integer selectOverlapCountDelYn(Integer eventUuid);
	
	
	Integer selectListReportDate(Integer eventUuid);
	Symposium selectListReportSymposium(Integer eventUuid);
	Symposium selectListReportSymposiumType(Integer eventUuid);
	Symposium selectListReportSymposiumDate(Integer eventUuid);
	Symposium selectSymposiumOption(Integer eventUuid);
	Integer updateSymposiumOption(Symposium symposium);
	
}
