package com.nori.hockey.persistence.mysql.mybatis.mapper;

import java.util.List;

import com.nori.hockey.domain.Brand;
import com.nori.hockey.domain.Company;
import com.nori.hockey.domain.Member;


public interface RegisterMapper {
	
	List<Company> selectListCompany();
	List<Brand> selectListBrand(Integer companyId);
	String selectMember(String userId);

	Integer selectCountCompany();
	Integer selectCountBrand(Integer companyId);
	
}
