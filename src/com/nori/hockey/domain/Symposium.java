package com.nori.hockey.domain;

import java.util.Date;

import org.apache.ibatis.type.Alias;

@Alias("Symposium")
public class Symposium {
	private Integer brandSeq = null;
	private Integer companySeq = null;
	private Integer eventId = null;
	private String eventOnOff = null;
	private String brandName = null;
	private String webinarType = null;
	private String preRegStartTime = null;
	private String preRegEndTime = null;
	private String eventName = null;
	private String eventPlace = null;
	private String eventStartTime = null;
	private String eventEndTime = null;
	private String presenter = null;
	private Integer preRegTempCount = null;
	private Integer preRegCount = null;
	private Integer individual = null;
	private Integer visit = null;
	private Integer delCount = null;
	private String siteId = null;

	public Integer getBrandSeq() {
		return brandSeq;
	}

	public void setBrandSeq(Integer brandSeq) {
		this.brandSeq = brandSeq;
	}

	public Integer getCompanySeq() {
		return companySeq;
	}

	public void setCompanySeq(Integer companySeq) {
		this.companySeq = companySeq;
	}

	public Integer getEventId() {
		return eventId;
	}

	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}

	public String getEventOnOff() {
		return eventOnOff;
	}

	public void setEventOnOff(String eventOnOff) {
		this.eventOnOff = eventOnOff;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public String getWebinarType() {
		return webinarType;
	}

	public void setWebinarType(String webinarType) {
		this.webinarType = webinarType;
	}

	public String getPreRegStartTime() {
		return preRegStartTime;
	}

	public void setPreRegStartTime(String preRegStartTime) {
		this.preRegStartTime = preRegStartTime;
	}

	public String getPreRegEndTime() {
		return preRegEndTime;
	}

	public void setPreRegEndTime(String preRegEndTime) {
		this.preRegEndTime = preRegEndTime;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public String getEventPlace() {
		return eventPlace;
	}

	public void setEventPlace(String eventPlace) {
		this.eventPlace = eventPlace;
	}

	public String getEventStartTime() {
		return eventStartTime;
	}

	public void setEventStartTime(String eventStartTime) {
		this.eventStartTime = eventStartTime;
	}

	public String getEventEndTime() {
		return eventEndTime;
	}

	public void setEventEndTime(String eventEndTime) {
		this.eventEndTime = eventEndTime;
	}

	public String getPresenter() {
		return presenter;
	}

	public void setPresenter(String presenter) {
		this.presenter = presenter;
	}

	public Integer getPreRegTempCount() {
		return preRegTempCount;
	}

	public void setPreRegTempCount(Integer preRegTempCount) {
		this.preRegTempCount = preRegTempCount;
	}

	public Integer getPreRegCount() {
		return preRegCount;
	}

	public void setPreRegCount(Integer preRegCount) {
		this.preRegCount = preRegCount;
	}

	public String getSiteId() {
		return siteId;
	}

	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}

	public Integer getIndividual() {
		return individual;
	}

	public void setIndividual(Integer individual) {
		this.individual = individual;
	}

	public Integer getVisit() {
		return visit;
	}

	public void setVisit(Integer visit) {
		this.visit = visit;
	}

	public Integer getDelCount() {
		return delCount;
	}

	public void setDelCount(Integer delCount) {
		this.delCount = delCount;
	}

	@Override
	public String toString() {
		return "Symposium [brandSeq=" + brandSeq + ", companySeq=" + companySeq + ", eventId=" + eventId
				+ ", eventOnOff=" + eventOnOff + ", brandName=" + brandName + ", webinarType=" + webinarType
				+ ", preRegStartTime=" + preRegStartTime + ", preRegEndTime=" + preRegEndTime + ", eventName="
				+ eventName + ", eventPlace=" + eventPlace + ", eventStartTime=" + eventStartTime + ", eventEndTime="
				+ eventEndTime + ", presenter=" + presenter + ", preRegTempCount=" + preRegTempCount + ", preRegCount="
				+ preRegCount + ", individual=" + individual + ", visit=" + visit + ", delCount=" + delCount
				+ ", siteId=" + siteId + "]";
	}

	

}
