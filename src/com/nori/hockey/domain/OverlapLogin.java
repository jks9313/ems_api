package com.nori.hockey.domain;

import java.util.Date;

import org.apache.ibatis.type.Alias;

@Alias("OverlapLogin")
public class OverlapLogin {
	private String type = null;
	private String session = null;
	private String userId = null;
	private String loginGubun = null;
	private String siteId = null;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getSession() {
		return session;
	}

	public void setSession(String session) {
		this.session = session;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getLoginGubun() {
		return loginGubun;
	}

	public void setLoginGubun(String loginGubun) {
		this.loginGubun = loginGubun;
	}

	public String getSiteId() {
		return siteId;
	}

	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}

	@Override
	public String toString() {
		return "OverlapLogin [type=" + type + ", session=" + session + ", userId=" + userId + ", loginGubun="
				+ loginGubun + ", siteId=" + siteId + "]";
	}

}
