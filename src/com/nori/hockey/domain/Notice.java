package com.nori.hockey.domain;

import java.util.Date;

import org.apache.ibatis.type.Alias;

@Alias("Notice")
public class Notice {

	private Integer rownum = null;
	private String noticeCd = null;
	private String noticeTitle = null;
	private String noticeContent = null;
	private String noticeFileExt = null;
	private String noticeFileName = null;
	private String noticeRegDate = null;
	private String creater = null;
	private String createdate = null;
	private String updater = null;
	private String updatedate = null;

	public Integer getRownum() {
		return rownum;
	}

	public void setRownum(Integer rownum) {
		this.rownum = rownum;
	}

	public String getNoticeCd() {
		return noticeCd;
	}

	public void setNoticeCd(String noticeCd) {
		this.noticeCd = noticeCd;
	}

	public String getNoticeTitle() {
		return noticeTitle;
	}

	public void setNoticeTitle(String noticeTitle) {
		this.noticeTitle = noticeTitle;
	}

	public String getNoticeContent() {
		return noticeContent;
	}

	public void setNoticeContent(String noticeContent) {
		this.noticeContent = noticeContent;
	}

	public String getNoticeFileExt() {
		return noticeFileExt;
	}

	public void setNoticeFileExt(String noticeFileExt) {
		this.noticeFileExt = noticeFileExt;
	}

	public String getNoticeFileName() {
		return noticeFileName;
	}

	public void setNoticeFileName(String noticeFileName) {
		this.noticeFileName = noticeFileName;
	}

	public String getNoticeRegDate() {
		return noticeRegDate;
	}

	public void setNoticeRegDate(String noticeRegDate) {
		this.noticeRegDate = noticeRegDate;
	}

	public String getCreater() {
		return creater;
	}

	public void setCreater(String creater) {
		this.creater = creater;
	}

	public String getCreatedate() {
		return createdate;
	}

	public void setCreatedate(String createdate) {
		this.createdate = createdate;
	}

	public String getUpdater() {
		return updater;
	}

	public void setUpdater(String updater) {
		this.updater = updater;
	}

	public String getUpdatedate() {
		return updatedate;
	}

	public void setUpdatedate(String updatedate) {
		this.updatedate = updatedate;
	}

	@Override
	public String toString() {
		return "Notice [rownum=" + rownum + ", noticeCd=" + noticeCd + ", noticeTitle=" + noticeTitle
				+ ", noticeContent=" + noticeContent + ", noticeFileExt=" + noticeFileExt + ", noticeFileName="
				+ noticeFileName + ", noticeRegDate=" + noticeRegDate + ", creater=" + creater + ", createdate="
				+ createdate + ", updater=" + updater + ", updatedate=" + updatedate + "]";
	}

}
