package com.nori.hockey.domain;

import java.util.Date;

import org.apache.ibatis.type.Alias;

@Alias("PreRegistration")
public class PreRegistration {
	private Integer eventUuid = null;
	private String name = null;
	private String hospital = null;
	private String specialty = null;
	private String hp = null;
	private String email = null;
	private String passcode = null;
	private String passCodeStatus = null;

	private Integer passcodeTmpCnt = null;
	private Integer passcodeCnt = null;
	private Integer delYnCnt = null;

	public Integer getEventUuid() {
		return eventUuid;
	}

	public void setEventUuid(Integer eventUuid) {
		this.eventUuid = eventUuid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getHospital() {
		return hospital;
	}

	public void setHospital(String hospital) {
		this.hospital = hospital;
	}

	public String getSpecialty() {
		return specialty;
	}

	public void setSpecialty(String specialty) {
		this.specialty = specialty;
	}

	public String getHp() {
		return hp;
	}

	public void setHp(String hp) {
		this.hp = hp;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPasscode() {
		return passcode;
	}

	public void setPasscode(String passcode) {
		this.passcode = passcode;
	}

	public String getPassCodeStatus() {
		return passCodeStatus;
	}

	public void setPassCodeStatus(String passCodeStatus) {
		this.passCodeStatus = passCodeStatus;
	}

	public Integer getPasscodeTmpCnt() {
		return passcodeTmpCnt;
	}

	public void setPasscodeTmpCnt(Integer passcodeTmpCnt) {
		this.passcodeTmpCnt = passcodeTmpCnt;
	}

	public Integer getPasscodeCnt() {
		return passcodeCnt;
	}

	public void setPasscodeCnt(Integer passcodeCnt) {
		this.passcodeCnt = passcodeCnt;
	}

	public Integer getDelYnCnt() {
		return delYnCnt;
	}

	public void setDelYnCnt(Integer delYnCnt) {
		this.delYnCnt = delYnCnt;
	}

	@Override
	public String toString() {
		return "PreRegistration [eventUuid=" + eventUuid + ", name=" + name + ", hospital=" + hospital + ", specialty="
				+ specialty + ", hp=" + hp + ", email=" + email + ", passcode=" + passcode + ", passCodeStatus="
				+ passCodeStatus + ", passcodeTmpCnt=" + passcodeTmpCnt + ", passcodeCnt=" + passcodeCnt + ", delYnCnt="
				+ delYnCnt + "]";
	}

}
