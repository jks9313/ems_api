package com.nori.hockey.domain;

import java.util.Date;

import org.apache.ibatis.type.Alias;

@Alias("Company")
public class Company {

	private Integer seq = null;
	private String id = null;
	private String name = null;
	private Integer owner = null;

	public Integer getSeq() {
		return seq;
	}

	public void setSeq(Integer seq) {
		this.seq = seq;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getOwner() {
		return owner;
	}

	public void setOwner(Integer owner) {
		this.owner = owner;
	}

	@Override
	public String toString() {
		return "Company [seq=" + seq + ", id=" + id + ", name=" + name + ", owner=" + owner + "]";
	}

}
