package com.nori.hockey.domain;

import java.util.Date;

import org.apache.ibatis.type.Alias;

@Alias("pdf")
public class Pdf {

	private Integer seqNo = 0;
	private String regName = "";
	private String regHospital = "";
	private String regSpecialty = "";
	private String regEmail = "";
	private String regHp = "";
	private String regHospitalAddress = "";
	private String regCompPhone = "";
	private String selectAgree1 = "";
	private String selectAgree2 = "";
	private String regDate = "";

	public Integer getSeqNo() {
		return seqNo;
	}

	public void setSeqNo(Integer seqNo) {
		this.seqNo = seqNo;
	}

	public String getRegName() {
		return regName;
	}

	public void setRegName(String regName) {
		this.regName = regName;
	}

	public String getRegHospital() {
		return regHospital;
	}

	public void setRegHospital(String regHospital) {
		this.regHospital = regHospital;
	}

	public String getRegSpecialty() {
		return regSpecialty;
	}

	public void setRegSpecialty(String regSpecialty) {
		this.regSpecialty = regSpecialty;
	}

	public String getRegEmail() {
		return regEmail;
	}

	public void setRegEmail(String regEmail) {
		this.regEmail = regEmail;
	}

	public String getRegHp() {
		return regHp;
	}

	public void setRegHp(String regHp) {
		this.regHp = regHp;
	}

	public String getRegHospitalAddress() {
		return regHospitalAddress;
	}

	public void setRegHospitalAddress(String regHospitalAddress) {
		this.regHospitalAddress = regHospitalAddress;
	}

	public String getRegCompPhone() {
		return regCompPhone;
	}

	public void setRegCompPhone(String regCompPhone) {
		this.regCompPhone = regCompPhone;
	}

	public String getSelectAgree1() {
		return selectAgree1;
	}

	public void setSelectAgree1(String selectAgree1) {
		this.selectAgree1 = selectAgree1;
	}

	public String getSelectAgree2() {
		return selectAgree2;
	}

	public void setSelectAgree2(String selectAgree2) {
		this.selectAgree2 = selectAgree2;
	}

	public String getRegDate() {
		return regDate;
	}

	public void setRegDate(String regDate) {
		this.regDate = regDate;
	}

	@Override
	public String toString() {
		return "Pdf [regName=" + regName + ", regHospital=" + regHospital + ", regSpecialty=" + regSpecialty
				+ ", regEmail=" + regEmail + ", regHp=" + regHp + ", regHospitalAddress=" + regHospitalAddress
				+ ", regCompPhone=" + regCompPhone + ", selectAgree1=" + selectAgree1 + ", selectAgree2=" + selectAgree2
				+ ", regDate=" + regDate + "]";
	}

}
