package com.nori.hockey.domain;

import java.util.Date;

import org.apache.ibatis.type.Alias;

@Alias("Brand")
public class Brand {

	private Integer seq = null;
	private String id = null;
	private String name = null;
	private String indecation = null;
	private Integer companySeq = null;

	public Integer getSeq() {
		return seq;
	}

	public void setSeq(Integer seq) {
		this.seq = seq;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIndecation() {
		return indecation;
	}

	public void setIndecation(String indecation) {
		this.indecation = indecation;
	}

	public Integer getCompanySeq() {
		return companySeq;
	}

	public void setCompanySeq(Integer companySeq) {
		this.companySeq = companySeq;
	}

	@Override
	public String toString() {
		return "Brand [seq=" + seq + ", id=" + id + ", name=" + name + ", indecation=" + indecation + ", companySeq="
				+ companySeq + "]";
	}

}
