package com.nori.hockey.domain;

import java.util.Date;

import org.apache.ibatis.type.Alias;

@Alias("Member")
public class Member {
	private Integer seq = null;
	private String userId = null;
	private String password = null;
	private String name = null;
	private String email = null;
	private String hp = null;
	private String organization = null;
	private String department = null;
	private String team = null;
	private String authority = null;
	private String memberCd = null;

	public Integer getSeq() {
		return seq;
	}

	public void setSeq(Integer seq) {
		this.seq = seq;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getHp() {
		return hp;
	}

	public void setHp(String hp) {
		this.hp = hp;
	}

	public String getOrganization() {
		return organization;
	}

	public void setOrganization(String organization) {
		this.organization = organization;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getTeam() {
		return team;
	}

	public void setTeam(String team) {
		this.team = team;
	}

	public String getAuthority() {
		return authority;
	}

	public void setAuthority(String authority) {
		this.authority = authority;
	}

	public String getMemberCd() {
		return memberCd;
	}

	public void setMemberCd(String memberCd) {
		this.memberCd = memberCd;
	}

	@Override
	public String toString() {
		return "Member [seq=" + seq + ", userId=" + userId + ", password=" + password + ", name=" + name + ", email="
				+ email + ", hp=" + hp + ", organization=" + organization + ", department=" + department + ", team="
				+ team + ", authority=" + authority + ", memberCd=" + memberCd + "]";
	}

}
