package com.nori.hockey.controller;

import java.util.List;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.nori.hockey.domain.Brand;
import com.nori.hockey.domain.Company;
import com.nori.hockey.domain.Member;
import com.nori.hockey.persistence.mysql.mybatis.mapper.RegisterMapper;

@RestController
@RequestMapping("/api/register")
public class RegisterController {
	
	@Autowired private RegisterMapper registerMapper;
	@Autowired private BCryptPasswordEncoder pwEncoder;

	@CrossOrigin
	@GetMapping("/company")
    public ResponseEntity<List<Company>> getCompany(){
		List<Company> list = registerMapper.selectListCompany();
		System.out.println(list.toString());
        return ResponseEntity.ok(list);
    }
	
	@CrossOrigin
	@GetMapping("/companyCount")
    public ResponseEntity<Integer> getCompanyCount(){
		Integer cnt = registerMapper.selectCountCompany();
		return ResponseEntity.ok(cnt);
    }
	
	@CrossOrigin
	@GetMapping("/brand/{companyId}")
    public ResponseEntity<List<Brand>> getBrand(@PathVariable("companyId") int companyId){
		System.out.println(companyId);
		List<Brand> list = registerMapper.selectListBrand(companyId);
        return ResponseEntity.ok(list);
    }

	@CrossOrigin
	@GetMapping("/brandCount")
    public ResponseEntity<Integer> getBrandCount(@RequestParam("id") int companyId){
        Integer cnt = registerMapper.selectCountBrand(companyId);
		return ResponseEntity.ok(cnt);
    }
	
	@CrossOrigin
	@GetMapping("/login")
    public ResponseEntity<JSONObject> getMember(@RequestParam("userId") String userId,@RequestParam("password") String password, @RequestParam("loginGubun") Integer loginGubun){
		Integer count = 0;
		String pw = null;
		if(loginGubun == 0) {
			pw = registerMapper.selectMember(userId);
			System.out.println(password);
			System.out.println(pw);
			System.out.println(pwEncoder.matches(password, pw));
			if(pwEncoder.matches(password, pw)) {
				count = 1;
			}
		}else if(loginGubun == 1){
			
		}
		JSONObject jsonObject = new JSONObject();
		 
        jsonObject.put("result", "OK");
        count = 1;
        jsonObject.put("data", count);
        return ResponseEntity.ok(jsonObject);
    }
	
	@CrossOrigin
	@PostMapping("/login")
    public ResponseEntity<JSONObject> postMember(@RequestParam("userId") String userId,@RequestParam("password") String password, @RequestParam("loginGubun") Integer loginGubun){
		Integer count = 0;
		String pw = null;
		if(loginGubun == 0) {
			pw = registerMapper.selectMember(userId);
			System.out.println(password);
			System.out.println(pw);
			System.out.println(pwEncoder.matches(password, pw));
			if(pwEncoder.matches(password, pw)) {
				count = 1;
			}
		}else if(loginGubun == 1){
			
		}
		JSONObject jsonObject = new JSONObject();
		 
        jsonObject.put("result", "OK");
        count = 1;
        jsonObject.put("data", count);
        return ResponseEntity.ok(jsonObject);
    }
	
	@CrossOrigin
	@GetMapping("/login2")
    public ResponseEntity<JSONObject> getMember2(){
		Integer count = 0;
		String pw = null;
		JSONObject jsonObject = new JSONObject();
		 
        jsonObject.put("result", "OK");
        count = 1;
        jsonObject.put("data", count);
        return ResponseEntity.ok(jsonObject);
    }
	
	@CrossOrigin
	@PostMapping("/login2")
    public ResponseEntity<JSONObject> postMembe2(){
		JSONObject jsonObject = new JSONObject();
		Integer count = 0;
        jsonObject.put("result", "OK");
        count = 1;
        jsonObject.put("data", count);
        return ResponseEntity.ok(jsonObject);
    }
}
