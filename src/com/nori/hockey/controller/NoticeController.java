package com.nori.hockey.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.nori.hockey.domain.Brand;
import com.nori.hockey.domain.Company;
import com.nori.hockey.domain.Member;
import com.nori.hockey.domain.Notice;
import com.nori.hockey.persistence.mysql.mybatis.mapper.NoticeMapper;
import com.nori.hockey.persistence.mysql.mybatis.mapper.RegisterMapper;

@RestController
@RequestMapping("/api/notice")
public class NoticeController {
	
	@Autowired NoticeMapper noticeMapper;
	
	@GetMapping("/noticeList")
    public ResponseEntity<List<Notice>> getNoticeList(){
		List<Notice> list = noticeMapper.selectListNotice();
        return ResponseEntity.ok(list);
    }
	
}
