package com.nori.hockey.controller;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.nori.Core.utility.StringUtil;
import com.nori.hockey.domain.Brand;
import com.nori.hockey.domain.Company;
import com.nori.hockey.domain.Member;
import com.nori.hockey.domain.OverlapLogin;
import com.nori.hockey.domain.PreRegistration;
import com.nori.hockey.domain.Symposium;
import com.nori.hockey.persistence.mysql.mybatis.mapper.OverlapLoginMapper;
import com.nori.hockey.persistence.mysql.mybatis.mapper.RegisterMapper;
import com.nori.hockey.persistence.mysql.mybatis.mapper.SymposiumMapper;
import com.nori.hockey.utility.ConversionPDF;

@RestController
@RequestMapping("/api/login")
public class OverlapLoginController {

	@Autowired
	OverlapLoginMapper overlapLoginMapper;

	@CrossOrigin
	@GetMapping("/overlapLChk")
	public JSONObject getOverlapLChk(OverlapLogin overlap) {
		List<OverlapLogin> list = overlapLoginMapper.getOverlapLChk(overlap);
		int temp = 0;
		for(int i = 0; i< list.size(); i++) {
			if(overlap.getSession().equals(list.get(i).getSession())){
				temp = 1;
				break;
			}
			
		}
		System.out.println(overlap.toString());
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("result", "OK");
		jsonObject.put("data", temp);

		return jsonObject;
	}

}