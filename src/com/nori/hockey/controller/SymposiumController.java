package com.nori.hockey.controller;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.nori.Core.utility.StringUtil;
import com.nori.hockey.domain.Brand;
import com.nori.hockey.domain.Company;
import com.nori.hockey.domain.Member;
import com.nori.hockey.domain.PreRegistration;
import com.nori.hockey.domain.Symposium;
import com.nori.hockey.persistence.mysql.mybatis.mapper.RegisterMapper;
import com.nori.hockey.persistence.mysql.mybatis.mapper.SymposiumMapper;
import com.nori.hockey.utility.ConversionPDF;

@RestController
@RequestMapping("/api/sympo")
public class SymposiumController {

	@Autowired
	SymposiumMapper symposiumMapper;

	@CrossOrigin
	@GetMapping("/symposiumList/{seq}/{gubun}")
	public JSONObject getSymposiumList(@PathVariable("seq") String seq,
			@PathVariable("gubun") Integer gubun, UriComponentsBuilder ucb) {
		List<Symposium> list = new ArrayList<>();
		if (gubun == 0) {
			// admin
			Integer userSeq = symposiumMapper.selectUserSeqByUserId(seq);
			list = symposiumMapper.selectListSymposiumByAdmin(userSeq);
		} else if (gubun == 1) {
			// user
			Symposium symposium = new Symposium();
			Integer val = Integer.parseInt(seq);
			symposium.setBrandSeq(val);
			list = symposiumMapper.selectListSymposiumByUser(symposium);
		}
        JSONObject jsonObject = new JSONObject();
 
        jsonObject.put("result", "OK");
        jsonObject.put("data", list);
        
		return jsonObject;
	}
	@CrossOrigin
	@PostMapping("/getSymposiumListPost")
	public JSONObject getSymposiumListPost(@RequestParam("seq") String seq,
			@RequestParam("gubun") Integer gubun) {
		List<Symposium> list = new ArrayList<>();
		if (gubun == 0) {
			// admin
			Integer userSeq = symposiumMapper.selectUserSeqByUserId(seq);
			list = symposiumMapper.selectListSymposiumByAdmin(userSeq);
		} else if (gubun == 1) {
			// user
			Symposium symposium = new Symposium();
			Integer val = Integer.parseInt(seq);
			symposium.setBrandSeq(val);
			list = symposiumMapper.selectListSymposiumByUser(symposium);
		}
        JSONObject jsonObject = new JSONObject();
 
        jsonObject.put("result", "OK");
        jsonObject.put("data", list);
        
		return jsonObject;
	}

	@CrossOrigin
	@GetMapping("/symposiumView/{eventUuid}")
	public JSONObject symposiumView(@PathVariable("eventUuid") Integer eventUuid) {
		Symposium symposium = new Symposium();
		symposium = symposiumMapper.selectSymposium(eventUuid);
		JSONObject jsonObject = new JSONObject();
		 
        jsonObject.put("result", "OK");
        jsonObject.put("data", symposium);
		return jsonObject;
	}
	@CrossOrigin
	@GetMapping("/symposiumPreRegWriteGet")
	public JSONObject symposiumPreRegWriteGet(PreRegistration preRegistration) {
		int j = symposiumMapper.insertPreRegTemp(preRegistration);
		System.out.println(preRegistration.toString());
		JSONObject jsonObject = new JSONObject();
		 
        jsonObject.put("result", "OK");
        jsonObject.put("data", j);
		return jsonObject;
	}
	@CrossOrigin
	@PostMapping("/symposiumPreRegWritePost")
	public JSONObject symposiumPreRegWritePost2(PreRegistration preRegistration) {
		System.out.println(preRegistration.toString());
		int j = symposiumMapper.insertPreRegTemp(preRegistration);
		
		JSONObject jsonObject = new JSONObject();
		 
        jsonObject.put("result", "OK");
        jsonObject.put("data", j);
		return jsonObject;
	}
	@CrossOrigin
	@GetMapping("/symposiumOption/{eventUuid}")
	public JSONObject symposiumPreRegWrite(@PathVariable("eventUuid") Integer eventUuid) {
		Symposium symposium = new Symposium();
		symposium = symposiumMapper.selectOptionPreReg(eventUuid);
		JSONObject jsonObject = new JSONObject();
		 
        jsonObject.put("result", "OK");
        jsonObject.put("data", symposium);
		return jsonObject;
	}

	@CrossOrigin
	@GetMapping("/symposiumPreRegList/{eventUuid}")
	public JSONObject selectListPreReg(@PathVariable("eventUuid") Integer eventUuid) {
		List<PreRegistration> preRegistration = symposiumMapper.selectListPreReg(eventUuid);
		
		JSONObject jsonObject = new JSONObject();
		 
        jsonObject.put("result", "OK");
        jsonObject.put("data", preRegistration);
		return jsonObject;
	}

	@CrossOrigin
	@GetMapping("/symposiumPreRegCount/{eventUuid}")
	public JSONObject selectCountPreReg(@PathVariable("eventUuid") Integer eventUuid) {
		PreRegistration preRegistration = symposiumMapper.selectCountPreReg(eventUuid);
		
		JSONObject jsonObject = new JSONObject();
		 
        jsonObject.put("result", "OK");
        jsonObject.put("data", preRegistration);
		return jsonObject;
	}

	@CrossOrigin
	@GetMapping("/selectOverlapCountPasscodeTemp/{eventUuid}")
	public JSONObject selectOverlapCountPasscodeTemp(@PathVariable("eventUuid") Integer eventUuid) {
		Integer result = symposiumMapper.selectOverlapCountPasscodeTemp(eventUuid);
		
		JSONObject jsonObject = new JSONObject();
		 
        jsonObject.put("result", "OK");
        jsonObject.put("data", result);
		return jsonObject;
	}

	@CrossOrigin
	@GetMapping("/selectOverlapCountPasscode/{eventUuid}")
	public JSONObject selectOverlapCountPasscode(@PathVariable("eventUuid") Integer eventUuid) {
		Integer result = symposiumMapper.selectOverlapCountPasscode(eventUuid);
		
		JSONObject jsonObject = new JSONObject();
		 
        jsonObject.put("result", "OK");
        jsonObject.put("data", result);
		return jsonObject;
	}

	@CrossOrigin
	@GetMapping("/selectOverlapCountDelYn/{eventUuid}")
	public JSONObject selectOverlapCountDelYn(@PathVariable("eventUuid") Integer eventUuid) {
		Integer result = symposiumMapper.selectOverlapCountDelYn(eventUuid);
		
		JSONObject jsonObject = new JSONObject();
		 
        jsonObject.put("result", "OK");
        jsonObject.put("data", result);
		return jsonObject;
	}

	@CrossOrigin
	@GetMapping("/selectListReportDate/{eventUuid}")
	public JSONObject selectListReportDate(@PathVariable("eventUuid") Integer eventUuid) {
		Integer result = symposiumMapper.selectListReportDate(eventUuid);
		
		JSONObject jsonObject = new JSONObject();
		 
        jsonObject.put("result", "OK");
        jsonObject.put("data", result);
		return jsonObject;
	}

	@CrossOrigin
	@GetMapping("/selectListReportSymposium/{eventUuid}")
	public JSONObject selectListReportSymposium(@PathVariable("eventUuid") Integer eventUuid) {
		Symposium symposium = new Symposium();
		symposium = symposiumMapper.selectListReportSymposium(eventUuid);
		
		JSONObject jsonObject = new JSONObject();
		 
        jsonObject.put("result", "OK");
        jsonObject.put("data", symposium);
		return jsonObject;
	}

	@CrossOrigin
	@GetMapping("/selectListReportSymposiumType/{eventUuid}")
	public JSONObject selectListReportSymposiumType(@PathVariable("eventUuid") Integer eventUuid) {
		Symposium symposium = new Symposium();
		symposium = symposiumMapper.selectListReportSymposiumType(eventUuid);
		
		JSONObject jsonObject = new JSONObject();
		 
        jsonObject.put("result", "OK");
        jsonObject.put("data", symposium);
		return jsonObject;
	}

	@CrossOrigin
	@GetMapping("/selectListReportSymposiumDate/{eventUuid}")
	public JSONObject selectListReportSymposiumDate(@PathVariable("eventUuid") Integer eventUuid) {
		Symposium symposium = new Symposium();
		symposium = symposiumMapper.selectListReportSymposiumDate(eventUuid);
		
		JSONObject jsonObject = new JSONObject();
		 
        jsonObject.put("result", "OK");
        jsonObject.put("data", symposium);
		return jsonObject;
	}

	@CrossOrigin
	@GetMapping("/selectSymposiumOption/{eventUuid}")
	public JSONObject selectSymposiumOption(@PathVariable("eventUuid") Integer eventUuid) {
		Symposium symposium = new Symposium();
		symposium = symposiumMapper.selectSymposiumOption(eventUuid);
		
		JSONObject jsonObject = new JSONObject();
		 
        jsonObject.put("result", "OK");
        jsonObject.put("data",symposium);
		return jsonObject;
	}

	@CrossOrigin
	@GetMapping("/updateSymposiumOption/{eventUuid}/{individual}/{visit}")
	public JSONObject updateSymposiumOption(@PathVariable("eventUuid") Integer eventUuid,
			@PathVariable("individual") Integer individual, @PathVariable("visit") Integer visit) {
		Symposium symposium = new Symposium();
		symposium.setEventId(eventUuid);
		symposium.setIndividual(individual);
		symposium.setVisit(visit);
		
		Integer result = symposiumMapper.updateSymposiumOption(symposium);
		
		JSONObject jsonObject = new JSONObject();
		 
        jsonObject.put("result", "OK");
        jsonObject.put("data", result);
		return jsonObject;
	}

}